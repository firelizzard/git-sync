/*
	Git Sync is a filesystem synchronization tool built on top of Git.
	Copyright 2019 The Git Sync Authors.

	This program is not affiliated with the Git project in any way.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/go-git/go-git/v5"
	"github.com/spf13/cobra"
)

var flags struct {
	repository string
}

var tickerFlags struct {
	period time.Duration
}

type notifierState struct {
	done    chan struct{}
	repo    *git.Repository
	watcher *fsnotify.Watcher
}

type tickerState struct {
	done     chan struct{}
	doneOnce sync.Once
	repo     *git.Repository
}

func main() { mainTicker() }

func mainNotifier() {
	cmd := &cobra.Command{
		Use:   "git-sync",
		Short: "Git Sync is a tool for synchronizing workspaces",
		RunE:  runNotifier,
	}

	cmd.Flags().StringVarP(&flags.repository, "repository", "r", ".", "Repository to synchronize")

	cmd.Execute()
}

func mainTicker() {
	cmd := &cobra.Command{
		Use:   "git-sync",
		Short: "Git Sync is a tool for synchronizing workspaces",
		RunE:  runTicker,
	}

	cmd.Flags().StringVarP(&flags.repository, "repository", "r", ".", "Repository to synchronize")
	cmd.Flags().DurationVarP(&tickerFlags.period, "period", "t", time.Minute, "Time between updates")

	cmd.Execute()
}

func runNotifier(cmd *cobra.Command, args []string) error {
	var err error
	var s = new(notifierState)

	s.repo, err = git.PlainOpen(flags.repository)
	if err != nil {
		return err
	}

	s.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer s.watcher.Close()

	s.done = make(chan struct{})
	go s.watch()

	err = s.add(flags.repository)
	if err != nil {
		return err
	}

	<-s.done
	return nil
}

func runTicker(cmd *cobra.Command, args []string) error {
	var err error
	var s = new(tickerState)

	s.repo, err = git.PlainOpen(flags.repository)
	if err != nil {
		return err
	}

	s.done = make(chan struct{})
	go s.tick()

	<-s.done
	return nil
}

func oops(err error) bool {
	if err == nil {
		return false
	}

	log.Println("error:", err)
	return true
}

func oopsf(format string, args ...interface{}) {
	oops(fmt.Errorf(format, args...))
}
