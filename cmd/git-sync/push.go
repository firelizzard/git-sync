/*
	Git Sync is a filesystem synchronization tool built on top of Git.
	Copyright 2019 The Git Sync Authors.

	This program is not affiliated with the Git project in any way.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport"
	git_ssh "github.com/go-git/go-git/v5/plumbing/transport/ssh"
	sshagent "github.com/xanzy/ssh-agent"
	"golang.org/x/crypto/ssh"
)

func push(repo *git.Repository) error {
	auth, err := findSSH(repo)
	if err != nil {
		return err
	}

	return repo.Push(&git.PushOptions{Auth: auth})
}

func findSSH(repo *git.Repository) (transport.AuthMethod, error) {
	head, err := repo.Head()
	if err != nil {
		return nil, err
	}

	if !head.Name().IsBranch() {
		return nil, nil
	}

	cfg, err := repo.Config()
	if err != nil {
		return nil, err
	}

	br, ok := cfg.Branches[head.Name().Short()]
	if !ok || br.Remote == "" {
		return nil, nil
	}

	remote, err := repo.Remote(br.Remote)
	if err != nil {
		return nil, err
	}

	agent, _, err := sshagent.New()
	if err != nil {
		return nil, err
	}

	for _, url := range remote.Config().URLs {
		ep, err := transport.NewEndpoint(url)
		if err != nil {
			return nil, err
		}

		if ep.Protocol == "ssh" && ep.Password == "" {
			keys, err := getSSHSigners(ep)
			if err != nil {
				return nil, err
			}

			if len(keys) == 0 {
				continue
			}

			return &git_ssh.PublicKeysCallback{
				User: ep.User,
				Callback: func() ([]ssh.Signer, error) {
					signers, err := agent.Signers()
					if err != nil {
						return nil, err
					} else if signers == nil {
						return keys, nil
					}
					return append(keys, signers...), nil
				},
			}, nil
		}
	}

	return nil, nil
}

func getSSHSigners(*transport.Endpoint) ([]ssh.Signer, error) {
	// TODO check ssh config

	home, err := os.UserHomeDir()
	if err != nil {
		return nil, fmt.Errorf("failed to locate SSH keys: failed to resolve user home directory: %v", err)
	}

	sshDir := filepath.Join(home, ".ssh")
	entries, err := ioutil.ReadDir(sshDir)
	if os.IsNotExist(err) {
		return []ssh.Signer{}, nil
	} else if err != nil {
		return nil, fmt.Errorf("failed to locate SSH keys: failed to read SSH directory: %v", err)
	}

	var signers []ssh.Signer
	for _, entry := range entries {
		if entry.IsDir() || !strings.HasPrefix(entry.Name(), "id_") || filepath.Ext(entry.Name()) != "" {
			continue
		}

		b, err := ioutil.ReadFile(filepath.Join(sshDir, entry.Name()))
		if err != nil {
			return nil, err
		}

		signer, err := ssh.ParsePrivateKey(b)
		if err != nil {
			return nil, err
		}

		signers = append(signers, signer)
	}

	return signers, nil
}
