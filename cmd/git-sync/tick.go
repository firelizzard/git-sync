/*
	Git Sync is a filesystem synchronization tool built on top of Git.
	Copyright 2019 The Git Sync Authors.

	This program is not affiliated with the Git project in any way.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"fmt"
	"time"

	"github.com/go-git/go-git/v5"
)

func (s *tickerState) tick() {
	defer s.doneOnce.Do(func() { close(s.done) })

	ticker := time.Tick(tickerFlags.period)

	for {
		select {
		case <-s.done:
			return

		case <-ticker:
			s.sync()
		}
	}
}

func (s *tickerState) sync() {
	wt, err := s.repo.Worktree()
	if oops(err) {
		return
	}

	st, err := wt.Status()
	if oops(err) || st.IsClean() {
		return
	}

	for file := range st {
		_, err = wt.Add(file)
		if oops(err) {
			return
		}
	}

	commit, err := wt.Commit("Updated on "+time.Now().String(), &git.CommitOptions{})
	if oops(err) {
		return
	}

	fmt.Printf("committed: %v... ", commit)

	err = push(s.repo)
	if err != nil {
		fmt.Println("push failed:", err)
		return
	}

	fmt.Println("pushed")
}
