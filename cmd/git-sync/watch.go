/*
	Git Sync is a filesystem synchronization tool built on top of Git.
	Copyright 2019 The Git Sync Authors.

	This program is not affiliated with the Git project in any way.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"
)

func (s *notifierState) add(root string) error {
	return filepath.Walk(root, walker(s.watcher.Add).walk)
}

func (s *notifierState) watch() {
	defer close(s.done)

	for {
		select {
		case <-s.done:
			return

		case event, ok := <-s.watcher.Events:
			if !ok {
				return
			}
			ts := time.Now()

			fmt.Println("event:", event.Op, event.Name)

			fi, err := os.Stat(event.Name)
			if os.IsNotExist(err) {
				s.sync(event.Name, false, ts)
				continue

			} else if oops(err) {
				continue
			}

			if fi.IsDir() {
				oops(s.add(event.Name))
				continue
			}

			s.sync(event.Name, true, ts)

		case err, ok := <-s.watcher.Errors:
			if !ok {
				return
			}
			oops(err)
		}
	}
}
