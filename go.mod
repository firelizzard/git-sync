module gitlab.com/firelizzard/git-sync

go 1.15

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-git/go-git/v5 v5.1.0
	github.com/spf13/cobra v1.0.0
	github.com/xanzy/ssh-agent v0.2.1
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
)
